import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import postService from "../api/postService";
import MyLoader from "../components/UI/loader/MyLoader";
import { useFetching } from "../hooks/useFetching";

const PostIdPage = () => {
    const params = useParams()
    const [post, setPost] = useState({});
    const [comments, setComments] = useState([]);
    const [fetchPostById, isLoading, error] = useFetching(async () => {
        const response = await postService.getById(params.id);
        setPost(response.data)
    })
    const [fetchComments, isCommentsLoading, commetsError] = useFetching(async () => {
        const response = await postService.getCommentsPostById(params.id);
        setComments(response.data)
    })

    useEffect(() => {
        fetchPostById(params.id);
        fetchComments(params.id);
    }, [])

    return (
        <div>
            <h1>You opened post page num {params.id}</h1>
            {isLoading
                ? <MyLoader />
                : <div>{post.title}</div>
            }
            <h1>Comments:</h1>
            {isCommentsLoading
                ? <MyLoader />
                : <div>
                    {comments.map(comment =>
                        <div key={comment.id} style={{ marginTop: 15 }}>
                            <h5>{comment.email}</h5>
                            <div>
                                {comment.body}
                            </div>
                        </div>
                    )}
                </div>
            }

        </div>
    )
}

export default PostIdPage;