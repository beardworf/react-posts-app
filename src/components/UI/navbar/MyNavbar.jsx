import React, { useContext } from "react"
import {
    Link
} from "react-router-dom";
import { AuthContext } from "../../../context";
import MyButton from "../button/MyButton";

const MyNavbar = () => {
    const { isAuth, setIsAuth } = useContext(AuthContext);

    const logout = () => {
        setIsAuth(false);
        localStorage.removeItem("auth");
    }

    return (
        <div className="navbar">
            <div className="navbar__links">
                <Link to="/about">About</Link>
                <Link to="/posts">Posts</Link>
            </div>
            <MyButton onClick={logout}>Logout</MyButton>
        </div>
    )
}

export default MyNavbar;