import React, { useEffect, useContext } from "react"
import { privateRoutes, publicRoutes } from "../router";
import { AuthContext } from "../context";
import {
    Routes,
    Route,
    useNavigate
} from "react-router-dom";
import MyLoader from "./UI/loader/MyLoader";


const AppRouter = () => {
    const { isAuth, isLoading } = useContext(AuthContext);

    const navigate = useNavigate();
    useEffect(() => {
        isAuth ? navigate("/posts") : navigate("/login")
    }, [isAuth]);

    if (isLoading) {
        return <MyLoader />
    }
    return (
        isAuth
            ? <Routes>
                {privateRoutes.map((route, index) =>
                    <Route
                        key={index}
                        element={route.element}
                        path={route.path}
                    />
                )}
            </Routes>
            : <Routes>
                {publicRoutes.map((route, index) =>
                    <Route
                        key={index}
                        element={route.element}
                        path={route.path}
                    />
                )}
            </Routes>
    )
}

export default AppRouter;